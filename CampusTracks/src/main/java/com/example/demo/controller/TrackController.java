package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Exception.CampusMindNotFoundException;
import com.example.demo.entity.Track;
import com.example.demo.service.TrackService;

@RestController
public class TrackController {
	
	@Autowired
	private TrackService trackService;
	
	@PostMapping("/addTrack")
	
	public Track addTrack(@RequestBody Track track) {
		return trackService.insertTrack(track);
	}
	
	@PutMapping("/updateDuration/track/{campusMindId}")
	public Track updateTrackDuration(@RequestBody Track track, @PathVariable("campusMindId") int campusMindId) throws CampusMindNotFoundException{
		return trackService.updateTrackDuration(track, campusMindId);
	}
	
	@GetMapping("/sort")
	public List<Track> sortTrackBasedOnName(){
		return trackService.sortTrackBasedOnName();
	}

	

}