package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Exception.CampusMindNotFoundException;
import com.example.demo.Exception.TrackNotFoundException;
import com.example.demo.entity.CampusMind;
import com.example.demo.service.CampusMindService;

@RestController
public class CampusMindController {
	
	@Autowired
	private CampusMindService campusMindService;
	 
	@PostMapping("/insert/campusMind/{trackId}")
	public CampusMind addCampusMind(@RequestBody CampusMind campusMind, @PathVariable("trackId") int trackId) throws TrackNotFoundException {
		return campusMindService.insertCampusMind(campusMind, trackId);
	}
	
	
	@PutMapping("/updateAge/{campusMindId}")
	public CampusMind updateCampusMindAge(@RequestBody CampusMind campusMind, @PathVariable("campusMindId")int campusMindId) throws CampusMindNotFoundException{
		return campusMindService.updateCampusMindAge(campusMind, campusMindId);
	}

}
