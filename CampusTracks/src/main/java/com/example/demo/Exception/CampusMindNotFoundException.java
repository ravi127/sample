package com.example.demo.Exception;

public class CampusMindNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public CampusMindNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CampusMindNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CampusMindNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CampusMindNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CampusMindNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
