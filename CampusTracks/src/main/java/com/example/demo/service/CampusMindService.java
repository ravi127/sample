package com.example.demo.service;

import org.springframework.stereotype.Service;

import com.example.demo.Exception.CampusMindNotFoundException;
import com.example.demo.Exception.TrackNotFoundException;
import com.example.demo.entity.CampusMind;

@Service
public interface CampusMindService {

	CampusMind insertCampusMind(CampusMind campusMind, int trackId) throws TrackNotFoundException;

	CampusMind updateCampusMindAge(CampusMind campusMind, int campusMindId) throws CampusMindNotFoundException;

}
