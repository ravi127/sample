package com.example.demo.service;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Exception.CampusMindNotFoundException;
import com.example.demo.Exception.TrackNotFoundException;
import com.example.demo.entity.CampusMind;
import com.example.demo.entity.Track;
import com.example.demo.repository.CampusMindRepository;
import com.example.demo.repository.TrackRepository;
@Service
public class CampusMindServiceImpl implements CampusMindService{
	
	@Autowired
	private TrackRepository trackRepository;
	
	@Autowired
	private CampusMindRepository campusMindRepository;

	@Override
	public CampusMind insertCampusMind(CampusMind campusMind, int trackId) throws TrackNotFoundException {
		Optional<Track> track = trackRepository.findById(trackId);
		if(!track.isPresent()) {
			throw new TrackNotFoundException("Track not found");
		}
		campusMind.setTrack(track.get());
		
		return campusMindRepository.save(campusMind);
	}

	@Override
	public CampusMind updateCampusMindAge(CampusMind campusMind, int campusMindId) throws CampusMindNotFoundException {
		Optional<CampusMind> campusMindFetched = campusMindRepository.findById(campusMindId);
		if(!campusMindFetched.isPresent()) {
			throw new CampusMindNotFoundException("CampusMind not found");
		}
		campusMindFetched.get().setAge(campusMind.getAge());
		
		return campusMindRepository.saveAndFlush(campusMindFetched.get());
	}
}
