package com.example.demo.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Exception.CampusMindNotFoundException;
import com.example.demo.entity.CampusMind;
import com.example.demo.entity.Track;
import com.example.demo.repository.CampusMindRepository;
import com.example.demo.repository.TrackRepository;

@Service
public class TrackServiceImpl implements TrackService {
	
	@Autowired
	private TrackRepository trackRepository;
	
	@Autowired
	private CampusMindRepository campusMindRepository;

	@Override
	public Track insertTrack(Track track) {
		
		return trackRepository.save(track);
	}

	@Override
	public Track updateTrackDuration(Track track, int campusMindId) throws CampusMindNotFoundException {
		Optional<CampusMind> campusMindFetch = campusMindRepository.findById(campusMindId);
		if(!campusMindFetch.isPresent()) {
			throw new CampusMindNotFoundException("CampusMind not found");
		}
		Optional<Track> trackFetched = trackRepository.findById(campusMindFetch.get().getCampusMindId());
		trackFetched.get().setTrackDuration(track.getTrackDuration());
		
		return trackRepository.saveAndFlush(trackFetched.get());
	}

	@Override
	public List<Track> sortTrackBasedOnName() {
		List<Track> tracks = trackRepository.findAll();
		Collections.sort(tracks);
		return tracks;
	}
	

}