package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.Exception.CampusMindNotFoundException;
import com.example.demo.entity.Track;

@Service
public interface TrackService {
	
	public Track insertTrack(Track track);

	public Track updateTrackDuration(Track track, int campusMindId) throws CampusMindNotFoundException;

	public List<Track> sortTrackBasedOnName();

}