package com.example.demo.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Track implements Comparable<Track> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int trackId;
	private String trackName;
	private String trackDuration;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "track")

	private List<CampusMind> campusMinds;

	public Track() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Track(int trackId, String trackName, String trackDuration, List<CampusMind> campusMinds) {
		super();
		this.trackId = trackId;
		this.trackName = trackName;
		this.trackDuration = trackDuration;
		this.campusMinds = campusMinds;
	}

	public int getTrackId() {
		return trackId;
	}

	public void setTrackId(int trackId) {
		this.trackId = trackId;
	}

	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

	public String getTrackDuration() {
		return trackDuration;
	}

	public void setTrackDuration(String trackDuration) {
		this.trackDuration = trackDuration;
	}

	public List<CampusMind> getCampusMinds() {
		return campusMinds;
	}

	public void setCampusMinds(List<CampusMind> campusMinds) {
		this.campusMinds = campusMinds;
	}
	
	

	@Override
	public String toString() {
		return "Track [trackId=" + trackId + ", trackName=" + trackName + ", trackDuration=" + trackDuration
				+ ", campusMinds=" + campusMinds + "]";
	}

	@Override
	public int compareTo(Track track) {
		if(this.trackName.compareToIgnoreCase(track.trackName)==0)
		return 0;
		else if(this.trackName.compareToIgnoreCase(track.trackName)>0)
		return 1;
		else
		return -1;
	}

	
}
