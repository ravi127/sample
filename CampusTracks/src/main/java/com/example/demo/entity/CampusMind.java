package com.example.demo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class CampusMind {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int campusMindId;
	private String mid;
	private String campusMindname;
	private int age;
	private String gender;
	@JsonIgnore
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Track track;

	public CampusMind() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CampusMind(int campusMindId, String mid, String campusMindname, int age, String gender, Track track) {
		super();
		this.campusMindId = campusMindId;
		this.mid = mid;
		this.campusMindname = campusMindname;
		this.age = age;
		this.gender = gender;
		this.track = track;
	}

	public int getCampusMindId() {
		return campusMindId;
	}

	public void setCampusMindId(int campusMindId) {
		this.campusMindId = campusMindId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getCampusMindname() {
		return campusMindname;
	}

	public void setCampusMindname(String campusMindname) {
		this.campusMindname = campusMindname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

}
