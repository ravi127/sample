package com.mindtree.Gym.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.mindtree.Gym.entity.Gym;
import com.mindtree.Gym.entity.Member;
import com.mindtree.Gym.exception.ServiceException;
import com.mindtree.Gym.service.GymService;
import com.mindtree.Gym.service.impl.GymServiceImpl;

public class GymDataManagementApp {
	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		List<Member> members = new ArrayList<Member>();
		List<Gym> gymList = new ArrayList<Gym>();
		GymService service = new GymServiceImpl();
		boolean isContinue = true;
		do {
			System.out.println("Enter option");
			int option = scan.nextInt();
			switch (option) {
			case 1:
				members = createMembers();
				try {
					String result = service.addMember(members);
					System.out.println(result);
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					System.out.println(e.getMessage());
				}
				break;
			case 2:
				System.out.println("Enter gym id");
				int gymid = scan.nextInt();
				try {
					List<Member> memberList = service.getFemalesSortedByName(gymid);
					displayreduceOperations(memberList);
					
					display.displayList(memberList);
					
				} catch (ServiceException | NullPointerException e) {
					// TODO Auto-generated catch block
					System.out.println("null pointer");
				}
				break;
			case 3:try {
					gymList=service.sortGymDetails();
					gymDisplay(gymList);
					displayNameInUppercasebelongstoparticularLocation(gymList);
					System.out.println();
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			case 4:
				System.out.println("Thank you");
				isContinue = false;
			default:
				System.out.println("Invalid Choice");

			}

		} while (isContinue);

	}

	public static List<Member> createMembers() {
		System.out.println("Enter number of members to add");
		int n = scan.nextInt();
		List<Member> membersList = new ArrayList<Member>();
		for (int i = 0; i < n; i++) {
			Member members = new Member();
			System.out.println("Enter member id");
			int id = scan.nextInt();
			System.out.println("Enter member name");
			String name = scan.next();
			System.out.println("Enter member age");
			int age = scan.nextInt();
			System.out.println("Enter member weight");
			float weight = scan.nextFloat();
			System.out.println("Enter member height");
			float height = scan.nextFloat();
			System.out.println("Enter gender");
			String gender = scan.next();
			System.out.println("Enter gymid");
			int gymid = scan.nextInt();
			members = new Member(id, name, age, weight, height, gender, gymid);
			membersList.add(members);
		}
		return membersList;
	}

	static  memberDisplay display=( memberList) -> {
		for (int i = 0; i < memberList.size(); i++) 
		{
			 System.out.println(memberList.get(i).getId()+" "+memberList.get(i).getName()+" "+memberList.get(i).getAge());
			
		}
		
	};
	public static void gymDisplay(List<Gym> gymList)
	{
		List<Integer> list=new ArrayList<Integer>();
		
		for(int i=0;i<gymList.size();i++)
		{
			//list = new ArrayList<Integer>();
			list.add(gymList.get(i).getId());
			System.out.println(gymList.get(i).getId()+" "+gymList.get(i).getGymname()+" "+gymList.get(i).getLocation());
			
		}
		
		int id=list.stream().reduce(Integer::sum).get();
		System.out.println("Sum of gym ids : "+id);
	}
	private static void displayNameInUppercasebelongstoparticularLocation(List<Gym> gymList)
	{
		System.out.println("Enter the location to get the gym details");
		//System.out.println(gymList);
		String location = scan.next();
		Stream<Object> gymName=gymList.stream().filter(gym->gym.getLocation().equalsIgnoreCase(location)).
				map(gym->gym.getGymname().toUpperCase());
				List<Object> gymList2=gymName.collect(Collectors.toList());
				System.out.println("Gym belongs to particular location:"+gymList2);
		
	}
	@FunctionalInterface
	interface memberDisplay
	{
		void displayList(List<Member> memberList);
	}
	public static void displayreduceOperations(List<Member> memberList)
	{
		List<Float> list=new ArrayList<Float>();
		for(int i=0;i<memberList.size();i++)
		{
			list.add(memberList.get(i).getHeight());
		}
		float avgheight=list.stream().reduce(Float::sum).get();
		System.out.println("avg height is"+avgheight/list.size());
		List<Integer> agelist=new ArrayList<Integer>();
		for(int i=0;i<memberList.size();i++)
		{
			agelist.add(memberList.get(i).getAge());
		}
		int smallage=agelist.stream().reduce(Integer::min).get();
		System.out.println("small age is"+smallage);
		List<Float> weightlist=new ArrayList<Float>();
		for(int i=0;i<memberList.size();i++)
		{
			weightlist.add(memberList.get(i).getWeight());
		}
		float maxweight=weightlist.stream().reduce(Float::max).get();
		System.out.println("max weight is"+maxweight);
		
		
		

	}
	
	
   

}
