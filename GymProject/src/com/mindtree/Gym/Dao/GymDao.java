package com.mindtree.Gym.Dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mindtree.Gym.entity.Gym;
import com.mindtree.Gym.entity.Member;
import com.mindtree.Gym.exception.DaoException;
import com.mindtree.Gym.exception.DataBaseConnectionFailedException;


public interface GymDao 
{
	public int createMember(List<Member> members) throws DaoException;
	default List<Member> getFemalesSortedByName(int gymid) throws DaoException
	{
		List numbers = Arrays.asList(3,7,8,1,5,9);
	   return numbers;	
	}
    List<Gym> sortGymDetails() throws DaoException;
}
