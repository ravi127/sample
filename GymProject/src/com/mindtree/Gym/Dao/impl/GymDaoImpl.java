package com.mindtree.Gym.Dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mindtree.Gym.Dao.GymDao;
import com.mindtree.Gym.entity.Gym;
import com.mindtree.Gym.entity.Member;
import com.mindtree.Gym.exception.DaoException;
import com.mindtree.Gym.exception.DataBaseConnectionFailedException;
import com.mindtree.Gym.utility.DbConnector;


public class GymDaoImpl implements GymDao {
	public int createMember(List<Member> members) throws DaoException
	{
		Connection conn = DbConnector.getMyConnection();
		PreparedStatement statement = null;
		ResultSet resultset=null;
		List<Member> memberList2 = new ArrayList<Member>();
		int j=0;
		ResultSet rs = null;
		if(conn!=null)
		{
			
        try {
        	for(int i=0;i<members.size();i++)
        		
        {
        	int gymid=  members.get(i).getGymid();
        	String query = "select * from gym.gymdetails where id="+gymid;
        	statement = conn.prepareStatement(query);
        	resultset = statement.executeQuery();
        	if(resultset.next())
        	{
			String sql = "{call insertmembers(?,?,?,?,?,?,?)}";
			CallableStatement callableStatement =  conn.prepareCall(sql);
			callableStatement.setInt(1, members.get(i).getId());
			callableStatement.setString(2, members.get(i).getName());
			callableStatement.setInt(3, members.get(i).getAge());
			callableStatement.setFloat(4, members.get(i).getWeight());
			callableStatement.setFloat(5, members.get(i).getHeight());
			callableStatement.setString(6, members.get(i).getGender());
			callableStatement.setInt(7,gymid);
			j=callableStatement.executeUpdate();
        	}
        	
		}
        }
        catch(SQLException e)
        {
        	System.out.println(e);
        }
        finally
        {
        	DbConnector.closeResource(conn);
        	DbConnector.closeResource(statement);
        	DbConnector.closeResource(rs);
        }
		}
		else
		{
			throw new DataBaseConnectionFailedException("Database is not connected ");
		}
		
		return j;
		
	}

	public List<Member> getFemalesSortedByName(int gymid) throws DaoException
	{
		List<Member> memberList = new ArrayList<Member>();
		Connection conn = DbConnector.getMyConnection();
		//PreparedStatement statement = null;
		ResultSet resultset=null;
		
		try {
			String sql ="{call displayfemales(?)}";
			CallableStatement callableStatement =  conn.prepareCall(sql);
			callableStatement.setInt(1, gymid);
			resultset=callableStatement.executeQuery();
//			if(resultset.next())
//			{
				while(resultset.next())
				{
					   Member member = new Member();
					   int id = resultset.getInt(1);
					    //member.setInt(resultset.getInt(1));
						String name = resultset.getString(2);
						int age = resultset.getInt(3);
						float weight = resultset.getFloat(4);
						float height = resultset.getFloat(5);
						String gender = resultset.getString(6);
						int gymId = resultset.getInt(7);
			            member = new Member(id,name,age,weight,height,gender,gymId);
			            memberList.add(member);
				}
			//}
			
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbConnector.closeResource(conn);
        	//DbConnection.closeResource(callableStatement);
        	DbConnector.closeResource(resultset);
		}
		
		return memberList;
	}
	
	@Override
	public List<Gym> sortGymDetails() throws DaoException
	{
		List<Gym> gymList = new ArrayList<Gym>();
		Connection conn = DbConnector.getMyConnection();
		PreparedStatement statement = null;
		ResultSet resultset=null;
		try
		{
			Gym gym = new Gym();
			String sqlQuery1 = "{call sortgym()}";
			CallableStatement callableStatement =  conn.prepareCall(sqlQuery1);
			resultset = callableStatement.executeQuery(sqlQuery1);
		while(resultset.next())
		{
		   int id = resultset.getInt(1);
		   String name = resultset.getString(2);
		   String location = resultset.getString(3);
		   gym = new Gym(id,name,location);
		   gymList.add(gym);
		}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbConnector.closeResource(conn);
        	DbConnector.closeResource(statement);
        	DbConnector.closeResource(resultset);
		}
		return gymList;
	}

	

}
