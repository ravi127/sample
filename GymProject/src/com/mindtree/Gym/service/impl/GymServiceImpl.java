package com.mindtree.Gym.service.impl;

import java.util.List;

import com.mindtree.Gym.Dao.GymDao;
import com.mindtree.Gym.Dao.impl.GymDaoImpl;
import com.mindtree.Gym.client.GymDataManagementApp;
import com.mindtree.Gym.entity.Gym;
import com.mindtree.Gym.entity.Member;
import com.mindtree.Gym.exception.DaoException;
import com.mindtree.Gym.exception.GymNotFoundException;
import com.mindtree.Gym.exception.NoFemalesFoundException;
import com.mindtree.Gym.exception.ServiceException;
import com.mindtree.Gym.service.GymService;

public class GymServiceImpl implements GymService 
{
    GymDao dao = new GymDaoImpl();
	@Override
	public String addMember(List<Member> members) throws ServiceException 
	{
		int result=0;
		String temp="";
		try {
			result = dao.createMember(members);
			if(result<=0)
			{
				throw new GymNotFoundException("Invalid gym id");
			}
			else
			{
				temp="data inserted";
			}
		} catch (DaoException e) 
		{
			e.printStackTrace();
		}
		catch(GymNotFoundException e)
		{
			System.out.println(e.getMessage());
		}
		return temp;
		
	}
	@Override
	public List<Member> getFemalesSortedByName(int gymid) throws ServiceException
	{
		List<Member> memberList=null;
		try {
			memberList=dao.getFemalesSortedByName(gymid);
			if(memberList.size()==0)
			{
				throw new NoFemalesFoundException(" Females Not Found");
		    }
		} 
		catch (DaoException e) 
		{
			e.printStackTrace();
		}
		catch(NoFemalesFoundException e)
		{
			System.out.println(e.getMessage());
		}
		
		return memberList;
	}
	@Override
	public List<Gym> sortGymDetails() throws ServiceException {
		// TODO Auto-generated method stub
		List<Gym> gymList=null;
		try {
			gymList=dao.sortGymDetails();
		} catch (DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return gymList;
	}
	

}
