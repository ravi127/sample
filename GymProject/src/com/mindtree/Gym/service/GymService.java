package com.mindtree.Gym.service;

import java.util.List;

import com.mindtree.Gym.entity.Gym;
import com.mindtree.Gym.entity.Member;
import com.mindtree.Gym.exception.DaoException;
import com.mindtree.Gym.exception.ServiceException;

public interface GymService 
{
	String addMember(List<Member> members) throws ServiceException;
	List<Member> getFemalesSortedByName(int gymid) throws ServiceException;
	List<Gym> sortGymDetails() throws ServiceException;
}
