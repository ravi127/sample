package com.mindtree.Gym.entity;

public class Gym 
{
	private int gymid;
	private String gymname;
	private String location;
	//private Member members;
	public Gym() {
		super();
	}
	public Gym(int id, String gymname, String location) {
		super();
		this.gymid = id;
		this.gymname = gymname;
		this.location = location;
	}
	public int getId() {
		return gymid;
	}
	public void setId(int id) {
		this.gymid = id;
	}
	public String getGymname() {
		return gymname;
	}
	public void setGymname(String gymname) {
		this.gymname = gymname;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	
	
	

}
