package com.mindtree.Gym.entity;

public class Member 
{
	private int id;
	private String name;
	private int age;
	private float weight;
	private float height;
	private String gender;
	private int gymid;
	public Member()
	{
		super();
	}
	public Member(int id, String name, int age, float weight, float height, String gender, int gymid) 
	{
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.gymid = gymid;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getGymid() {
		return gymid;
	}
	public void setGymid(int gymid) {
		this.gymid = gymid;
	}
	

}
