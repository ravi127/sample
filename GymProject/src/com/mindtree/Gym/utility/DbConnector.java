package com.mindtree.Gym.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mindtree.Gym.exception.DataBaseConnectionFailedException;
import com.mysql.cj.jdbc.CallableStatement;

public interface DbConnector 
{
	 static final String URL = "jdbc:mysql://localhost:3306/Gym";
	 static final String USER_NAME = "root";
	 static final String PASSWORD = "1A2b3c@#";

	public static Connection getMyConnection() throws DataBaseConnectionFailedException {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
		} catch (SQLException e) {
			throw new DataBaseConnectionFailedException("Database could not connect", e);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return con;
	}

	public static void closeResource(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void closeResource(PreparedStatement ps) {
		if (ps != null) {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public static void callableStatementClose(CallableStatement callableStatement) {
		if (callableStatement != null)
		try {
			callableStatement.close(); 
		} catch (SQLException ignore) {}
		}

	public static void closeResource(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void closeResource(Statement st) {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}


}
