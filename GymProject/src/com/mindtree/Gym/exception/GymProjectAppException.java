package com.mindtree.Gym.exception;

public class GymProjectAppException extends Exception
{

	public GymProjectAppException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GymProjectAppException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public GymProjectAppException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GymProjectAppException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GymProjectAppException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}
