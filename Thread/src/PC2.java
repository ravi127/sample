
public class PC2 {

	public static void main(String[] args) 
	{
		Queue1 q=new Queue1();
		Producer1 p=new Producer1(q);
		Consumer1 c=new Consumer1(q);
		p.start();
		c.start();
	}
}
class Queue1
{
	int x;
	boolean value_in_x=false;
	synchronized public void put(int i)
	{
		if(value_in_x==true)
		{
			try {wait();} catch (InterruptedException e) {}
		}
		else
		{
			x=i;
			System.out.println("Chef has produced food"+" "+x);
			value_in_x=true;
			notify();
		}
	}
	synchronized public void get()
	{
		if(value_in_x==false)
		{
			try {wait();} catch (InterruptedException e) {}
		}
		else
		{
			System.out.println("Person has consumed food"+" "+x);
			value_in_x=false;
			notify();
		}
	}
}
class Producer1 extends Thread
{
	Queue1 a;
	public Producer1(Queue1 m)
	{
		a=m;
	}
	public void run()
	{
		int j=0;
		while(true)
		{
			a.put(j++);
			
			
		}
		
		
	}
}
class Consumer1 extends Thread
{
	Queue1 b;
	public Consumer1(Queue1 n)
	{
		b=n;
	}
	public void run()
	{
		while(true)
		{
			b.get(); 
			
			
		}
		
		
	}
}
