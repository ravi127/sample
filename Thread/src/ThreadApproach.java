

import java.util.Scanner;

public class ThreadApproach 
{
	public static void main(String[] args) 
	{
		Demo1 d1=new Demo1();
		Demo2 d2=new Demo2();
		Demo3 d3=new Demo3();
		d1.start();
		d2.start();
		d3.start();
	}
}
class Demo1 extends Thread
{
	public void run()
	{
		System.out.println("Banking process started");
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter bank account no");
		int bankno=scan.nextInt();
		System.out.println("Enter password");
		int password=scan.nextInt();
		try 
		{
			Thread.sleep(1000);
		}
		catch (InterruptedException e) 
		{	
			e.printStackTrace();
		}
		System.out.println("collect money");
		System.out.println("Banking activity finished");
	}
}
class Demo2 extends Thread
{
	public void run()
	{
		System.out.println("Num Printing started");
		for(int i=0;i<5;i++)
		{
			System.out.println(i);
			
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
			
			
		}
		System.out.println("Num printing ended");
	}
}
class Demo3 extends Thread
{
	public void run()
	{
		System.out.println("char Printing started");
		for(int i=0;i<5;i++)
		{
			System.out.println((char)(i+65));
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("char printing ended");
	}
}