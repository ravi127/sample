
public class PrintLetter 
{
	private static Object lock = new Object();
	 
    private static Thread th1 = new Thread() {
        @Override
        public void run() {
            synchronized (lock) {
                for (int i = 1; i <= 5; i++) {
                    System.out.println(i);
                    lock.notify();
                    try {
                        Thread.sleep(1000);
                        lock.wait();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
    private static Thread th2 = new Thread() {
        @Override
        public void run() {
            synchronized (lock) {
                for (int i = 10; i >= 6; i--) {
                    System.out.println(i);
                    lock.notify();
                    try {
                        Thread.sleep(1000);
                        lock.wait();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
 
    public static void main(String[] args) {
        th1.start();
        th2.start();
    }

}
