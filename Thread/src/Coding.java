
public class Coding
{
	public static void main(String[] args) 
	{
		PrintMethod p = new PrintMethod();
		Demo d = new Demo(p);
		Thread t1 = new Thread(d);
		Thread2 t2 = new Thread2(p);
		t1.start();
		t2.start();
		
	}

}
class PrintMethod
{
	boolean valueset = false;
	synchronized void print1(int i)
	{
		while(valueset)
		{
			try
			{
			wait();
			}
			catch(Exception e)
			
			{
				
			}
		}
		System.out.println(i);
		valueset=true;
		notify();
		
	}
	synchronized void print10(int i)
	{
		while(!valueset)
		{
			try
			{
				wait();
			}
			catch(Exception e)
			{
				
			}
		}
		System.out.println(i);
		valueset=false;
		notify();
	}
	
}
class Demo implements Runnable
{
	PrintMethod a;
	public Demo(PrintMethod p)
	{
		a=p;
	}
	
	
	public void run()
	{
		for(int i=10;i>=6;i--)
		{
			a.print10(i);
			try
			{
				Thread.sleep(1000);
			}
			catch(Exception e)
			{
				
			}
			
		}
		
	}
}
class Thread2 extends Thread
{
	PrintMethod a;
	public Thread2(PrintMethod p)
	{
		a=p;
	}
	public void run()
	{
		for(int i=1;i<=5;i++)
		{
			a.print1(i);
			try
			{
				Thread.sleep(1000);
			}
			catch(Exception e)
			{
				
			}
		}
		
	}
}

