

public class Synchronied 
{
	public static void main(String[] args)
	{
		Bathroom b=new Bathroom();
		Thread t1=new Thread(b);
		Thread t2=new Thread(b);
		Thread t3=new Thread(b);
		t1.setName("PERSON1");
		t2.setName("PERSON2");
		t3.setName("PERSON3");
		t1.start();
		t2.start();
		t3.start();
	}
}
class Bathroom implements Runnable
{

	@Override
	synchronized public void run() {
		try 
		{
			Thread t=Thread.currentThread();
			String name=t.getName();
			System.out.println(name + " has entered bathroom");
			Thread.sleep(1000);
			System.out.println(name + " is using bathroom");
			Thread.sleep(1000);
			System.out.println(name + " exited bathroom");
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Activity interrupted");
		}
		
	}
	
}
