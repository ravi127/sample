package com.mindtree.Gym.dao;

import com.mindtree.Gym.exception.GymDaoException;

public interface GymDao 
{
	void addGym(int id,String name,String location) throws GymDaoException;
    void addMembers(int id,String name,int age,double weight,double height,String gender,int gymId) throws GymDaoException;
}
