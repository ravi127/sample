package com.mindtree.Gym.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mindtree.Gym.exception.GymDaoException;
import com.mindtree.Gym.utility.UtilDb;

public class GymDaoImpl implements GymDao {

	@Override
	public void addGym(int id, String name, String location) throws GymDaoException
	{
		// TODO Auto-generated method stub
		       UtilDb util = new UtilDb();
				Connection con = util.getMyConnection();;
				
				Statement st = null;
				ResultSet rs = null;
				PreparedStatement ps = null;
				if(con!=null)
				{
					System.out.println("Connected");
				}
				else
				{
					System.out.println("Not connected");
				}
				try {
					List<Integer> list=new ArrayList<Integer>();
					con =  util.getMyConnection();
					st = con.createStatement();
					String query="select * from GymDetails";
					rs=st.executeQuery(query);
					while(rs.next()) {
						list.add(rs.getInt(1));
					}
					int count=0;
					for(Integer a:list) {
						if(a==id) {
							count++;
							System.out.println("Gymy alredy exist");
						}
					}
						if(count==0) {
							String query1="INSERT INTO GymDetails(id,name,location) VALUES ('"+id+"','"+name+"','"+location+"')";
							st.execute(query1);
							System.out.println("Data added");
						}
					
					
				}catch(SQLException e) {
					System.out.println(e);
				}
				finally {
					util.closeResource(con);
					util.closeResource(st);
					util.closeResource(rs);
					// util.closeResource(ps);
				}
				
		
	}

	@Override
	public void addMembers(int id, String name, int age, double weight, double height, String gender, int gymId)
			throws GymDaoException 
	{
		
				Connection con = null;
				Statement st = null;
				ResultSet rs = null;
				PreparedStatement ps = null;
				UtilDb util = new UtilDb();
				try {
					con=util.getMyConnection();
					List<Integer> list=new ArrayList<Integer>();
					st = con.createStatement();
					String query="select * from Members";
					rs=st.executeQuery(query);
					while(rs.next()) {
						list.add(rs.getInt(1));
					}
					int count=0;
					for(Integer a:list) {
						if(a==gymId)
							count++;
					}
					if(count==1) {
						String query1="INSERT INTO Member(Id,Name,age,weight,height,gender,gymId) VALUES ('"+id+"','"+name+"','"+age+"','"+weight+" "+height+" "+gender+" "+gymId+ ")";
						st.execute(query1);
						System.out.println("Data added");
					}
					else
						System.out.println("Gym not found to add "+name+" book");
				}catch(SQLException e) {
					System.out.println(e);
					
				}finally {
					util.closeResource(con);
					util.closeResource(st);
					util.closeResource(rs);
					// util.closeResource(ps);
				}
		

	}

}
