package com.mindtree.Gym.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mindtree.Gym.exception.DataBaseConnectionFailedException;

public class UtilDb 
{
	private final String URL = "jdbc:mysql://localhost:3306/Gym";
	private final String USER_NAME = "root";
	private final String PASSWORD = "1A2b3c@#";

	public Connection getMyConnection() throws DataBaseConnectionFailedException {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
		} catch (SQLException e) {
			throw new DataBaseConnectionFailedException("Database could not connect", e);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return con;
	}

	public void closeResource(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void closeResource(PreparedStatement ps) {
		if (ps != null) {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void closeResource(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void closeResource(Statement st) {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
