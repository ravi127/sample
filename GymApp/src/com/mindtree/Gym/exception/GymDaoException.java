package com.mindtree.Gym.exception;

public class GymDaoException extends GymApplicationException
{

	public GymDaoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GymDaoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public GymDaoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GymDaoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GymDaoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}
