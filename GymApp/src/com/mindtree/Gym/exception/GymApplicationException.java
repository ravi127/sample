package com.mindtree.Gym.exception;

public class GymApplicationException extends Exception
{

	public GymApplicationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GymApplicationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public GymApplicationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GymApplicationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GymApplicationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}
