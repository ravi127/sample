package com.mindtree.Gym.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.mindtree.Gym.entity.Gym;
import com.mindtree.Gym.entity.Member;
import com.mindtree.Gym.exception.GymServiceException;
import com.mindtree.Gym.service.GymService;
import com.mindtree.Gym.service.GymServiceImpl;

public class GymDataManagementApp 
{
	static Scanner scan = new Scanner(System.in);
	static GymService service=new GymServiceImpl();
	public static void main(String[] args) 
	{
		boolean isContinue=true;
		do
		{
		System.out.println("1)Add gym \n2)Add members \n3)Search Gym \n4)sorted gyms");
		System.out.println("Enter option");
		int option = scan.nextInt();
		switch(option)
		{
		case 1:addGyms();
		break;
		case 2:addMembers();
		break;
		}
		}while(isContinue);
		
	}
	public static void addGyms()
	{
		
		System.out.println("Enter Gym id");
		int id = scan.nextInt();
		System.out.println("Enter gym name");
		String name = scan.next();
		System.out.println("Enter location");
		String location = scan.next();
		try
		{
			service.addGym(id, name, location);
		}
		catch(GymServiceException e)
		{
			System.out.println(e);
		}
		
	}
	public static void addMembers()
	{
		
		System.out.println("Enter id");
		int id = scan.nextInt();
		System.out.println("Enter name");
		String name = scan.next();
		System.out.println("Enter age");
		int age = scan.nextInt();
		System.out.println("Enter weight");
		double weight = scan.nextDouble();
		System.out.println("Enter Height");
		double height = scan.nextDouble();
		System.out.println("Enter gender");
		String gender = scan.next();
		System.out.println("Enter the gymid");
		int gymId =  scan.nextInt();
		try
		{
			service.addMembers(id, name, age, weight, height, gender, gymId);
		}
		catch(GymServiceException e)
		{
			System.out.println(e);
		}
		
		
	}
	




}
