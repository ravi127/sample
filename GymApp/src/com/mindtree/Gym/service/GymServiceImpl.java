package com.mindtree.Gym.service;

import com.mindtree.Gym.dao.GymDao;
import com.mindtree.Gym.dao.GymDaoImpl;
import com.mindtree.Gym.exception.GymDaoException;
import com.mindtree.Gym.exception.GymServiceException;

public class GymServiceImpl implements GymService

{
	static GymDao dao=new GymDaoImpl();
	@Override
	public void addGym(int id, String name, String location) throws GymServiceException 
	{
		try
		{
			dao.addGym(id, name, location);
			
		}
		catch(GymDaoException e)
		{
			System.out.println(e);
		}
		
		
	}

	@Override
	public void addMembers(int id, String name, int age, double weight, double height, String gender, int gymId)
			throws GymServiceException {
		try
		{
			dao.addMembers(id, name, age, weight, height, gender, gymId);
			
		}
		catch(GymDaoException e)
		{
			System.out.println(e);
		}
		
	}
	

}
