package com.mindtree.Gym.service;

import com.mindtree.Gym.exception.GymDaoException;
import com.mindtree.Gym.exception.GymServiceException;

public interface GymService 
{
	void addGym(int id,String name,String location) throws GymServiceException;
	void addMembers(int id,String name,int age,double weight,double height,String gender,int gymId) throws GymServiceException;

}
