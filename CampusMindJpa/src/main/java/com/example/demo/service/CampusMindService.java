package com.example.demo.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.example.demo.entity.CampusMind;
import com.example.demo.entity.Track;
import com.example.demo.exception.serviceexception.ServiceException;

public interface CampusMindService 
{
	public Track addTrack(Track list) throws ServiceException;
     public CampusMind registerMinds(CampusMind list) throws ServiceException;
     public List<Track> getAllTracks() throws ServiceException;
     //public CampusMind assignMindToTrack(int id,int tid) throws ServiceException;
//     public List<CampusMind> getAllMinds();
      public Optional<CampusMind> deleteMind(int id) throws ServiceException;
      public List<Track> sortAllTracks() throws ServiceException;
      public List<CampusMind> sortAllCampusMinds() throws ServiceException;
     public Optional<Track> getTrackById(int tid) throws ServiceException;
     public Map<Integer,String> getTrackIdName();
     public Optional<Track> deleteTrack(int id) throws ServiceException;

}
