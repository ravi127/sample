package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.demo.entity.CampusMind;
import com.example.demo.entity.Track;
import com.example.demo.exception.serviceexception.DuplicateMindException;
import com.example.demo.exception.serviceexception.DuplicateTrackException;
import com.example.demo.exception.serviceexception.MindNotFoundException;
import com.example.demo.exception.serviceexception.ServiceException;
import com.example.demo.exception.serviceexception.TrackNotFoundException;
import com.example.demo.repository.CampusMindRepository;
import com.example.demo.repository.TrackRepository;
import com.example.demo.service.CampusMindService;

@Service
public class CampusMindServiceImpl implements CampusMindService
{
	@Autowired 
	private TrackRepository trackrepo;
	@Autowired
	private CampusMindRepository mindrepo;
	public Track addTrack(Track list) throws ServiceException
	{
		List<Track> data = trackrepo.findAll();
		for(int i=0;i<data.size();i++)
		{
			if(list.getName().equals(data.get(i).getName()))
			{
				throw new DuplicateTrackException("Duplicate Track");
			}
		}
		return trackrepo.save(list);
	}
	@Override
	public CampusMind registerMinds(CampusMind list) throws ServiceException 
	{
		List<CampusMind> data = mindrepo.findAll();
		for(int i=0;i<data.size();i++)
		
		{
			
			if(list.getName().equals(data.get(i).getName()))
			{
				throw new DuplicateMindException("Duplicate mind");
			}
		}
		
		return mindrepo.save(list);
	}
//	@Override
	//public CampusMind assignMindToTrack(int id, int tid) throws ServiceException {
//		CampusMind mind=mindrepo.findByid(id);
//		Track track=trackrepo.findById(tid);
//	List<Track> data=trackrepo.findAll();
//	if(!data.contains(track))
//		{
//			throw new TrackNotFoundException("track Not Found");
//	}
//	mind.setTrack(track);
//		return mindrepo.save(mind);
//	
//	}
	@Override
	@Cacheable("fetchTracks")
	public List<Track> getAllTracks() throws ServiceException 
	{
		List<Track> data = trackrepo.findAll();
		if(data==null)
		{
			throw new TrackNotFoundException("track Not Found");
		}
		
		return data;
	}
	@Override
	public Optional<CampusMind> deleteMind(int id) throws ServiceException {
		List<CampusMind> minds = mindrepo.findAll();
		Optional<CampusMind> mind=mindrepo.findById(id);
		mind.orElseThrow(() -> new MindNotFoundException("Mind Not Found"));
	
		mindrepo.deleteById(id);
		return mind;
		
	}
	@Override
	public List<Track> sortAllTracks() throws ServiceException {
		List<Track> tracks = trackrepo.findAll();
		Collections.sort(tracks);
		return tracks; 
		
		
	}
	@Override
	public List<CampusMind> sortAllCampusMinds() throws ServiceException
	{
		List<CampusMind> mindList = new ArrayList<CampusMind>();
	mindList=mindrepo.findAll();
		Comparator<CampusMind> mindNameComparator = new Comparator<CampusMind>() {

			@Override
			public int compare(CampusMind o1, CampusMind o2) {
				if(o1.getAge()==o2.getAge())
				{
			if(o1.getName().equals(o2.getName()))
				return 0;
			else if(o1.getName().compareTo(o2.getName())>0)
				return 1;
			else
				return -1;
			}
				else
				{
				 if(o1.getAge()<o2.getAge())
				return 1;
				else 
					return -1;
				}
			}
			
		};
	     
		Collections.sort(mindList,mindNameComparator);
		
		return mindList;
	}
	@Override
	public Optional<Track> getTrackById(int tid) throws ServiceException
	{
		List<Track> tracks = trackrepo.findAll();
		Optional<Track> track=trackrepo.findById(tid);
		track.orElseThrow(() -> new TrackNotFoundException("Track Not Found"));
		
		return trackrepo.findById(tid);
	}
	@Override
	public Map<Integer, String> getTrackIdName() 
	{
		List<Track> tlist=trackrepo.findAll();
		Map<Integer,String> list= new HashMap<Integer,String>() ;
		for(Track t: tlist)
		{
			list.put(t.getTid(), t.getName());
		}
		return list;
	}
	@Override
	public Optional<Track> deleteTrack(int id) throws ServiceException {
		
		List<Track> tracks = trackrepo.findAll();
		Optional<Track> track=trackrepo.findById(id);
		track.orElseThrow(() -> new TrackNotFoundException("Track Not Found"));
	
		trackrepo.deleteById(id);
		return track;
	}
	
	

}
