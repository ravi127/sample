package com.example.demo.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Track implements Comparable<Track>
{
	@Id
	private int tid;
	private String name;
	private int duration;
	@JsonIgnore
	@OneToMany(mappedBy="track")
	private List<CampusMind> minds;
	
	public Track() {
		super();
	}
	public Track(int tid, String name, int duration, List<CampusMind> minds) {
		super();
		this.tid = tid;
		this.name = name;
		this.duration = duration;
		this.minds = minds;
	}
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public List<CampusMind> getMinds() {
		return minds;
	}
	public void setMinds(List<CampusMind> minds) {
		this.minds = minds;
	}
	@Override
	public int compareTo(Track track)
	{
		
		if(name.equals(track.name))
			return 0;
		else if(name.compareToIgnoreCase(track.name)>0)

			return 1;
		else
			return -1;
		
		
		
	}
	@Override
	public String toString() {
		return "Track [tid=" + tid + ", name=" + name + "]";
	}
	
	
	

}
