package com.example.demo.entity;

import java.util.Comparator;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class CampusMind
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name;
	private int age;
	private String gender;
	@ManyToOne
	@JoinColumn(name="tid")
	private Track track;
	
	public CampusMind() {
		super();
	}
	public CampusMind(int id, String name, int age, String gender, Track track) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.track = track;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Track getTrack() {
		return track;
	}
	public void setTrack(Track track2) {
		this.track = track2;
	}
	
	
	

}
