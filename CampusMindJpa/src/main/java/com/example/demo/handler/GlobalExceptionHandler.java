package com.example.demo.handler;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.demo.exception.CampusMindApplicationException;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler 
{

Map<String,Object> response;
	
	@ExceptionHandler(value= {CampusMindApplicationException.class})
	protected ResponseEntity<Map<String,Object>> 
	handleConflict(CampusMindApplicationException ex,WebRequest request)
	{
		response = new HashMap<String,Object>();
		response.put("message",ex.getMessage());
		response.put("Status",HttpStatus.BAD_REQUEST);
		response.put("body",null);
		response.put("error",true);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}

}
