package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.CampusMind;

@Repository
public interface CampusMindRepository extends JpaRepository<CampusMind,Integer>
{
	public Optional<CampusMind> findById(int id);
    public CampusMind findByid(int id);
}
