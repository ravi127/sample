package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Track;

@Repository
public interface TrackRepository extends JpaRepository<Track,Integer>
{
	public Optional<Track> findById(int tid);

}
