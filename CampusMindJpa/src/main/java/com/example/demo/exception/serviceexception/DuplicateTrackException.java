package com.example.demo.exception.serviceexception;

public class DuplicateTrackException extends ServiceException {

	public DuplicateTrackException() {
		// TODO Auto-generated constructor stub
	}

	public DuplicateTrackException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DuplicateTrackException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DuplicateTrackException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DuplicateTrackException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
