package com.example.demo.exception.serviceexception;

public class DuplicateMindException extends ServiceException {

	public DuplicateMindException() {
		// TODO Auto-generated constructor stub
	}

	public DuplicateMindException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DuplicateMindException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DuplicateMindException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DuplicateMindException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
