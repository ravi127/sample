package com.example.demo.exception;

public class CampusMindApplicationException extends Exception
{

	public CampusMindApplicationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CampusMindApplicationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CampusMindApplicationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CampusMindApplicationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CampusMindApplicationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}
