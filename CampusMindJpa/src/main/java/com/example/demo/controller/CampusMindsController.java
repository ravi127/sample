package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.CampusMind;
import com.example.demo.entity.Track;
import com.example.demo.exception.CampusMindApplicationException;
import com.example.demo.service.CampusMindService;

@RestController
public class CampusMindsController 
{
	private Map<String,Object> response;
	@Autowired
	private CampusMindService service;
	@PostMapping("/track")
	public ResponseEntity<Map<String, Object>> addTracks(@RequestBody Track list) throws CampusMindApplicationException
	{
		response = new HashMap<String,Object>();
		response.put("message", "Tracks added succesfully");
		response.put("status",HttpStatus.OK);
		response.put("body", service.addTrack(list));
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		
	}
	@PostMapping("/addMind")
	public ResponseEntity<Map<String, Object>> addMinds(@RequestBody CampusMind list) throws CampusMindApplicationException 
	{
		response = new HashMap<String,Object>();
		response.put("message", "Minds added succesfully");
		response.put("status",HttpStatus.OK);
		response.put("body", service.registerMinds(list));
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		
	}
//	@PutMapping("/addMindtoTracks/{id}/{tid}")
//	public ResponseEntity<Map<String, Object>> addMindsToTrack(@PathVariable("id") int id,@PathVariable("tid") int tid) throws CampusMindApplicationException 
//	{
//		response = new HashMap<String,Object>();
//		response.put("message", "Minds assigned to track succesfully");
//		response.put("status",HttpStatus.OK);
//		response.put("body", service.assignMindToTrack(id, tid));
//		response.put("error", false);
//		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
//		
//	}
	@GetMapping("/track")
	public ResponseEntity<Map<String, Object>> getAllTracks() throws CampusMindApplicationException 
	{
		response = new HashMap<String,Object>();
		response.put("message", "Displaying All Tracks");
		response.put("status",HttpStatus.OK);
		response.put("body", service.getAllTracks());
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		
	}
	@DeleteMapping("/deleteMind/{id}")
	public ResponseEntity<Map<String, Object>> deleteMindById(@PathVariable("id") int id) throws CampusMindApplicationException 
	{
		response = new HashMap<String,Object>();
		response.put("message", "Deleted Mind");
		response.put("status",HttpStatus.OK);
		response.put("body", service.deleteMind(id));
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		
	} 
	@GetMapping("/sortTracks")
	public ResponseEntity<Map<String, Object>>sortAllTracks() throws CampusMindApplicationException 
	{
		response = new HashMap<String,Object>();
		response.put("message", "Displaying All Tracks");
		response.put("status",HttpStatus.OK);
		response.put("body", service.sortAllTracks());
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		
	}
	@GetMapping("/sortMinds")
	public ResponseEntity<Map<String, Object>>sortAllMinds() throws CampusMindApplicationException 
	{
		response = new HashMap<String,Object>();
		response.put("message", "Displaying All Minds");
		response.put("status",HttpStatus.OK);
		response.put("body", service.sortAllCampusMinds());
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		
	}
	@PostMapping("/track/{tid}")
	public ResponseEntity<Map<String, Object>> getTrackById(@PathVariable("tid") int tid) throws CampusMindApplicationException 
	{
		response = new HashMap<String,Object>();
		response.put("message", "Displaying All Tracks");
		response.put("status",HttpStatus.OK);
		response.put("body", service.getTrackById(tid));
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		
	}
	@GetMapping("/getIdName")
	public ResponseEntity<Map<String, Object>> getTrackIdName() throws CampusMindApplicationException 
	{
		response = new HashMap<String,Object>();
		response.put("message", "Displaying All Minds");
		response.put("status",HttpStatus.OK);
		response.put("body", service.getTrackIdName());
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		
	}
	@DeleteMapping("/deleteTrack/{tid}")
	public ResponseEntity<Map<String, Object>> deleteTrack(@PathVariable("tid") int tid) throws CampusMindApplicationException 
	{
		response = new HashMap<String,Object>();
		response.put("message", "Displaying All Minds");
		response.put("status",HttpStatus.OK);
		response.put("body", service.deleteTrack(tid));
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		
	}
	
	
	

}
